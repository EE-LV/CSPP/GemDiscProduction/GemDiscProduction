Release Notes for the Gem Disk Production Project - 14-March-2017
=================================================
This LabVIEW project "GemDiskProduction.lvproj" is used to develop the Software zur Steuerung einer Produktionsanlage für gewickelte Faserverbundwerkstoffe, welche im Rahmen des PANDA-Projekts Anwendung finden wird.

- GemDiskProduction will be based on native LabVIEW classes and the NI Actor Framework and CS++.

Version 0.0.0.0
============
The project was just started. There is the master branch with license only.
