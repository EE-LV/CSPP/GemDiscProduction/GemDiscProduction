﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">Gem Disk Production

Software zur Steuerung einer Produktionsanlage für gewickelte Faserverbundwerkstoffe, welche im Rahmen des PANDA-Projekts Anwendung finden wird.

Copyright 2017 GSI Helmholtzzentrum für Schwerionenforschung GmbH

Daniel Krebs, H.Brand, Planckstr. 1, 64291 Darmstadt, Germany.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{00A98648-75A5-4020-B270-A989D71EFA58}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_ResourceName</Property>
	<Property Name="varPersistentID:{01C9E2FB-3F13-498E-9805-9097769DD3F0}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_dtAI</Property>
	<Property Name="varPersistentID:{0297A1FB-F4D0-48A1-8ABE-5AEE99191002}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/Disc Size U</Property>
	<Property Name="varPersistentID:{03EDCEE4-55EA-4262-9E23-7E01FD23F4BE}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-Controller_PollingInterval</Property>
	<Property Name="varPersistentID:{04310445-2B6E-47E9-BA66-355EAFD3B054}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_ModuleFault_0</Property>
	<Property Name="varPersistentID:{07032862-8A4D-44C2-B392-CF94E672E246}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActorProxy_Activate</Property>
	<Property Name="varPersistentID:{07899EE9-714A-409F-85CC-14AFF48488F7}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{078AEA4B-07B7-40D4-B41D-711B9878A691}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-Velocity_2</Property>
	<Property Name="varPersistentID:{0C94F242-0788-455B-B7F3-5ED14322013C}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_PollingTime</Property>
	<Property Name="varPersistentID:{0D655349-42EF-4A01-A191-0243E6796532}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/LNA Name</Property>
	<Property Name="varPersistentID:{11C352A9-3D54-4A6D-B90B-E6F81093F931}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_SelftestResultCode</Property>
	<Property Name="varPersistentID:{15545961-3414-41B6-A5F4-FFB6BFEDBDB9}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceProxy_Activate</Property>
	<Property Name="varPersistentID:{15AA8E21-EAEE-4D72-BC89-61E253525F11}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_T_0</Property>
	<Property Name="varPersistentID:{16A5255E-8778-4F6C-9F4A-9678930BF347}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-ControllerProxy_Activate</Property>
	<Property Name="varPersistentID:{17465408-5C2B-4E32-9295-ACD8C17E738C}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AI_1</Property>
	<Property Name="varPersistentID:{1F12C293-E121-4819-AFA1-6D3AD7FE5334}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/LNA Return Msg</Property>
	<Property Name="varPersistentID:{1F20D810-D3E2-49FF-9724-37C547B33198}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Velocity_0</Property>
	<Property Name="varPersistentID:{22D03F4E-9A8B-46AC-A3A8-4F5DA4ADC637}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{23473CE4-2106-408C-BF86-561D199A036C}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_SelfTest</Property>
	<Property Name="varPersistentID:{23D295EF-120E-46BC-9FAD-3BB45849CE19}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_PID-Kc</Property>
	<Property Name="varPersistentID:{23FD58B7-B204-419C-9D10-C5AFC4B58D59}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{252A0E4C-6EDC-41D9-BBA4-A1F5DC9F1C06}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_PID-Td</Property>
	<Property Name="varPersistentID:{26F28B26-83E0-4F54-A299-EE40093317F4}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Enable_1</Property>
	<Property Name="varPersistentID:{280C327B-DDAE-4A2A-8C40-2690DB821FB7}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-Acceleration_2</Property>
	<Property Name="varPersistentID:{294E7034-5359-456A-8ADF-2D6124EA46F6}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_ModuleFault_1</Property>
	<Property Name="varPersistentID:{2F09D0D8-DAA7-4CCC-8AAF-C211C1E26097}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/Watchdog Status</Property>
	<Property Name="varPersistentID:{2F74A38D-2495-41A7-9E67-252A7F525E39}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-AO_3</Property>
	<Property Name="varPersistentID:{2FB28BFB-40F1-4BB8-AEBA-A03038A5BB63}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AI_2</Property>
	<Property Name="varPersistentID:{30C25490-8DA7-406A-8A06-413723BE2874}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_SpeedRegister</Property>
	<Property Name="varPersistentID:{3CC79042-DDBF-4204-ACFA-E67B759295A3}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{3DBE06E8-FFDF-4342-B83B-B3791F2BD951}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/Disc Size V</Property>
	<Property Name="varPersistentID:{3F8E1DC6-9AEC-4CFD-9AE1-F1582838535B}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_dtTemperature</Property>
	<Property Name="varPersistentID:{405B18BC-E825-4B70-ADB5-2F161FA3D449}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{423D2F96-857E-47CE-BAE2-77023990849E}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_RPM</Property>
	<Property Name="varPersistentID:{43C95AE6-DF85-4108-8037-C46B51ACC538}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_Set-PID-Td</Property>
	<Property Name="varPersistentID:{43DE1389-E221-40D1-A216-D0BC0B1F5528}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Acceleration_1</Property>
	<Property Name="varPersistentID:{498EB903-496E-41D0-BC35-14326B013CF0}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302Proxy_Activate</Property>
	<Property Name="varPersistentID:{49E66860-C203-45B9-BAA2-E71B2DFE686D}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-DO</Property>
	<Property Name="varPersistentID:{4AA088E3-F207-4161-85F9-74F752388F3B}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Position</Property>
	<Property Name="varPersistentID:{4D5AB7EC-BFE6-4792-BBC9-72303ED71888}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-Controller_ErrorCode</Property>
	<Property Name="varPersistentID:{57075461-05CE-42BC-A2E4-105B85F1305D}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-Velocity_1</Property>
	<Property Name="varPersistentID:{574B3969-1AEE-4F95-9464-DCAA4FB81B1F}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_T_3</Property>
	<Property Name="varPersistentID:{57E5CACE-7A93-42DE-A204-3C184E714349}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-Controller_PollingMode</Property>
	<Property Name="varPersistentID:{57FDD244-AF47-43D8-9C72-D1B72ECC11F5}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/Disc Free V</Property>
	<Property Name="varPersistentID:{58CD4155-CACD-4EA0-8E5A-31E350F0BD26}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_DriveStatus_2</Property>
	<Property Name="varPersistentID:{58EEC349-534D-4DB2-9C11-60FD49DB5370}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Reset-Position</Property>
	<Property Name="varPersistentID:{5B803E54-78C3-4542-9EEC-F52A8A86B833}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_FirmwareRevision</Property>
	<Property Name="varPersistentID:{616FE9FC-0B7E-4E2B-8445-984824DC8AEF}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_PID-Ti</Property>
	<Property Name="varPersistentID:{618D1A18-BD09-499B-A4DD-382C52D10496}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-Acceleration_0</Property>
	<Property Name="varPersistentID:{6342806D-763A-492D-BD33-5AE613D88D73}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_DriverRevision</Property>
	<Property Name="varPersistentID:{669A112E-B4CA-4CA4-B028-CE832A9DAAEA}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_PollingStartStop</Property>
	<Property Name="varPersistentID:{66A1F54F-D7A0-4B6C-A1C0-EA1593A54D6C}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_PollingStartStop</Property>
	<Property Name="varPersistentID:{6867061B-4106-441F-9F56-C42ABE996411}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_Set-RPM</Property>
	<Property Name="varPersistentID:{6A4F2F91-0E6E-4EF0-B211-FD5ADA3AAA59}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_SelftestResultCode</Property>
	<Property Name="varPersistentID:{6AD1D160-DC25-4F68-8CBF-369DCFB35D50}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{6C3E1110-8C73-4EE2-8503-9140C0157006}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_ResourceName</Property>
	<Property Name="varPersistentID:{6D5676FA-DA61-4F99-99EB-2A0626EBB63C}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{6E827B05-FC03-48EB-8E34-CFAEDC466C11}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_ErrorMessage</Property>
	<Property Name="varPersistentID:{7083BC5A-8101-4112-85D5-19ACBA1F6F72}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{71470D4E-6BA7-46A6-93C5-2D4833D53E67}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_dtAO</Property>
	<Property Name="varPersistentID:{74854E3F-A78F-4596-AD54-251C7542EF27}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AO_3</Property>
	<Property Name="varPersistentID:{77221A9B-77D0-49B0-9402-C0B1A7AB44EC}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_Enable</Property>
	<Property Name="varPersistentID:{7B645D6F-A508-4325-A996-315505F278B1}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Enable_2</Property>
	<Property Name="varPersistentID:{7CBEB894-CC56-4E0F-920C-42666173B956}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{7DDCF953-47CB-4B2F-AF57-91F9AE285A7F}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{7EC3D4E1-EE74-4328-B823-6A8DB4961EBB}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_Enabled</Property>
	<Property Name="varPersistentID:{7F60D6FF-BB81-4740-9D99-0204F6E31DEB}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_PID-Enabled</Property>
	<Property Name="varPersistentID:{81E2DAC7-6AC9-483B-BF8D-E2A3E50BDFB7}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/Disc Size C</Property>
	<Property Name="varPersistentID:{854C0BD8-FE28-4717-B9B6-CD6BDFE8B8BE}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{879E818A-F0D9-4065-A0DB-B20FE4975CA7}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_ModuleFault_2</Property>
	<Property Name="varPersistentID:{8BF00811-23A7-4DC6-B7A1-58B258FB08F6}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-Controller_PollingTime</Property>
	<Property Name="varPersistentID:{8DFEF5A9-E5BA-4B77-B112-958E06099881}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_Set-PID-Enable</Property>
	<Property Name="varPersistentID:{90D99CBF-5FF6-4959-A161-3AABC816B4BC}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_DriverRevision</Property>
	<Property Name="varPersistentID:{9199F3CA-F800-4EBF-8653-66983B1AEF97}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_DI</Property>
	<Property Name="varPersistentID:{92F9858C-D90D-4A6F-8294-6BA1245EB9CD}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_PollingInterval</Property>
	<Property Name="varPersistentID:{930A009E-FEB1-4067-9991-B5A0F95AD96D}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-ControllerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{95EF3835-806B-4DC3-97C0-345AA47F2580}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_T_2</Property>
	<Property Name="varPersistentID:{97D70AF2-4921-4130-920A-7235CADBFB4B}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{9882B501-BB4D-483B-B539-16442E83121C}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_ErrorMessage</Property>
	<Property Name="varPersistentID:{99BAE46E-6834-4ACE-9A5F-02050BBDE799}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{9BB07E25-C4A7-474D-8E3F-022D06A7A145}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{9C33064E-00CE-4DF6-8BD7-714BEEAE73C2}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/LNA Ping Counter</Property>
	<Property Name="varPersistentID:{9CB23753-6E3F-4261-B24D-7EDACF34BD00}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Enable_0</Property>
	<Property Name="varPersistentID:{9CE25947-4ED8-4FD1-8347-652FBAC14A72}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{A04802C4-9B62-4330-8A0A-B41CDC07EA68}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{A10A1138-1140-44DF-9829-9A73B61C492B}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_PollingInterval</Property>
	<Property Name="varPersistentID:{A5A02F11-8094-40B7-9876-A351E4297CFB}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-Controller_ErrorMessage</Property>
	<Property Name="varPersistentID:{A7B48B7F-057C-4A5D-919D-657EF8FE266A}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{A8D744E9-F0F4-4AD3-9E06-E1BB756249CD}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{A96F2E8C-365E-4793-B787-68765F6F7540}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-Controller_PollingStartStop</Property>
	<Property Name="varPersistentID:{AC96F56E-E86A-4E95-96F6-49E0B740BEDD}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-AO_1</Property>
	<Property Name="varPersistentID:{ADA1DC89-946C-4294-BB88-66BE0D3C0AAE}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Velocity_1</Property>
	<Property Name="varPersistentID:{B1D15ECE-05DD-4AF9-98B7-6D52FAD01AF5}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Acceleration_2</Property>
	<Property Name="varPersistentID:{B658213D-256E-4BC0-97D7-E68BE664E669}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_DriveStatus_1</Property>
	<Property Name="varPersistentID:{B73E843C-AE1E-4530-8C7D-31B539786CE8}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Round</Property>
	<Property Name="varPersistentID:{B7B72928-06F9-4CC2-B248-35371882146A}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_PollingMode</Property>
	<Property Name="varPersistentID:{BA35CDC0-5830-4F32-B14F-7EF7425133BE}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Velocity_2</Property>
	<Property Name="varPersistentID:{BB3DCC18-672E-47B4-988B-39F400D2651D}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{BC9A0973-432C-4B4F-9F84-DD55B24FD920}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-AO_2</Property>
	<Property Name="varPersistentID:{BCF944A9-21B3-4EF6-9545-278BF2F6BE23}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_SelfTest</Property>
	<Property Name="varPersistentID:{BEF2DDE6-BB74-4621-8C1E-8DF23A224442}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGAProxy_Activate</Property>
	<Property Name="varPersistentID:{C3E60614-3A7E-4E81-B338-0DB03A0DA513}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/LNA Reader</Property>
	<Property Name="varPersistentID:{C8F96B99-65C5-410D-961C-0BB37D69756E}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/Disc Free C</Property>
	<Property Name="varPersistentID:{C90F3B36-5D92-4BBC-A91C-7246FF2748E6}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_dtDIO</Property>
	<Property Name="varPersistentID:{C920DAE4-69FB-4859-AE91-27C389FA8838}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_Set-PID-Kc</Property>
	<Property Name="varPersistentID:{CCB5FE89-51DD-4415-AF7A-84C5F7055F09}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_DO</Property>
	<Property Name="varPersistentID:{CF0F5C04-F5EA-4163-A331-67D3611B2E9C}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_DriveStatus_0</Property>
	<Property Name="varPersistentID:{CFB56B5D-7D42-4656-82FF-950DAA77A3F4}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AI_3</Property>
	<Property Name="varPersistentID:{D07C03CD-6F15-4E44-85E0-57227A0092D7}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_T_1</Property>
	<Property Name="varPersistentID:{D1032EB4-6594-4365-A071-2F249A980F70}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_StatusRegister</Property>
	<Property Name="varPersistentID:{D1A6515B-B3CA-4897-8CA0-E19BF6113F7D}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AO_0</Property>
	<Property Name="varPersistentID:{D1F5841C-D1F2-4C7B-9E5D-8B23CE35D828}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_SelfTest</Property>
	<Property Name="varPersistentID:{D2ACBF71-5FF0-458D-9347-8C8E7D05F595}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_FirmwareRevision</Property>
	<Property Name="varPersistentID:{D39D30E2-CBBB-43F1-98F9-844190C73164}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGAProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{D7A25139-51C0-47EA-9EC5-7A7381579FC9}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/Disc Free U</Property>
	<Property Name="varPersistentID:{D8F88B41-C44D-4D08-95D6-7A7BB77AE466}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-Velocity_0</Property>
	<Property Name="varPersistentID:{D91D7C4F-E380-4437-A7F4-2B7C0D603E89}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_PollingMode</Property>
	<Property Name="varPersistentID:{D9C4F0DB-FDEC-4EFA-8358-0E42D421D4CC}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_dtEncoder</Property>
	<Property Name="varPersistentID:{DBD90FF3-B9FE-4B0B-9CAA-ADD71D339491}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/System Status</Property>
	<Property Name="varPersistentID:{E07E8BAD-568A-406C-BCFA-796C047C6B74}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_dtMotion</Property>
	<Property Name="varPersistentID:{E16331AA-5F7D-45EA-8E85-AED2B1F25E04}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-TensileStressActor_Set-PID-Ti</Property>
	<Property Name="varPersistentID:{E4BDD9E6-A0AA-4BA5-9854-0B57FB1EEF52}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-AO_0</Property>
	<Property Name="varPersistentID:{E62075CD-1292-4615-8288-70CEDB407682}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AI_0</Property>
	<Property Name="varPersistentID:{E799530A-01AA-463E-84D7-AEBBA969671E}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myBaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{E7FD88C8-28FA-4CDF-AB28-5A008882C37A}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Laenge</Property>
	<Property Name="varPersistentID:{E885F34E-91F3-4925-BFB6-9ABE90755E0E}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_ErrorCode</Property>
	<Property Name="varPersistentID:{E903AECA-A8DA-4D29-B10E-A18D671726D0}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AO_2</Property>
	<Property Name="varPersistentID:{E9D8EEA2-C897-4BCD-B42A-3B52F70DFECC}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-RTSystemHealth-SV.lvlib/LNA Writer</Property>
	<Property Name="varPersistentID:{E9DD5BA2-CD90-4515-BC8E-CC2C11E27947}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{EDF7F3A8-9680-4DE5-8183-852376F37519}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_Reset</Property>
	<Property Name="varPersistentID:{F1AE20AB-04C2-40D3-B8E1-2E2EFECA53CB}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FC302_PollingTime</Property>
	<Property Name="varPersistentID:{F1D4AEFF-3546-4CBF-A11A-C9262C3F5B97}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_ErrorCode</Property>
	<Property Name="varPersistentID:{F55AC0AE-3C14-4160-AC1E-A295590ED354}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_AO_1</Property>
	<Property Name="varPersistentID:{F6B3C660-EBCF-4A83-8E61-828D61E84F59}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Set-Acceleration_1</Property>
	<Property Name="varPersistentID:{F8FA39BF-1023-4C57-9664-1B2A6720D5F9}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Acceleration_0</Property>
	<Property Name="varPersistentID:{FA69C4B4-479A-467D-ADF3-8DB2E7E56D43}" Type="Ref">/RT CompactRIO Target/GDP/GDP-RT-SV.lvlib/GDP-FPGA_Reset</Property>
	<Property Name="varPersistentID:{FEAE34E8-D0C5-4D3D-BE49-FE0E79729F35}" Type="Ref">/RT CompactRIO Target/CS++/RT/CSPP-Core-SV-RT.lvlib/myDeviceActor_Reset</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="GDP-3NI9503-Stepper-Drive-Host.vi" Type="VI" URL="../Packages/GDP-NI9503-Stepper-Drive/GDP-3NI9503-Stepper-Drive-Host.vi"/>
		<Item Name="GemDiscProduction.ini" Type="Document" URL="../GemDiscProduction.ini"/>
		<Item Name="PID Zugspannung.vi" Type="VI" URL="../Packages/GDP_RT/GDP_HMI/PID Zugspannung.vi"/>
		<Item Name="README.md" Type="Document" URL="../README.md"/>
		<Item Name="Release_Notes.md" Type="Document" URL="../Release_Notes.md"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="lvpidtkt.dll" Type="Document" URL="/&lt;vilib&gt;/addons/control/pid/lvpidtkt.dll"/>
				<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				<Item Name="NI_PID__prctrl compat.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/control/pid/NI_PID__prctrl compat.lvlib"/>
				<Item Name="NI_PID_pid.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/control/pid/NI_PID_pid.lvlib"/>
				<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
			</Item>
			<Item Name="GDP-9503-Support.lvlib" Type="Library" URL="../Packages/GDP-NI9503-Stepper-Drive/GDP-9503-Support.lvlib"/>
			<Item Name="GDP-9503.lvlib" Type="Library" URL="../Packages/GDP-NI9503-Stepper-Drive/GDP-9503.lvlib"/>
			<Item Name="GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx" Type="Document" URL="../Packages/GDP-NI9503-Stepper-Drive/FPGA Bitfiles/GDP-NI9503-Stepp_FPGATarget_Main_DoDqZffLYJo.lvbitx"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Velocity Conversion StepsPerPeriod to RPS.vi" Type="VI" URL="../Packages/GDP-NI9503-Stepper-Drive/support/Velocity Conversion StepsPerPeriod to RPS.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="RT CompactRIO Target" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">RT CompactRIO Target</Property>
		<Property Name="alias.value" Type="Str">140.181.89.218</Property>
		<Property Name="CCSymbols" Type="Str">OS,Linux;CPU,ARM;DeviceCode,76D6;LaunchDeviceActor,False;TARGET_TYPE,RT;</Property>
		<Property Name="crio.ControllerPID" Type="Str">76D6</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">8</Property>
		<Property Name="host.TargetOSID" Type="UInt">8</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/home/lvuser/natinst/bin/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">true</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/home/lvuser/natinst/bin</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Batch Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
		</Item>
		<Item Name="CS++" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Libs" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
				</Item>
			</Item>
			<Item Name="LNA" Type="Folder">
				<Item Name="CSPP_LNA.lvlib" Type="Library" URL="../Packages/CSPP_LNA/CSPP_LNA.lvlib"/>
				<Item Name="Linked Network Actor.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI/Actors/Linked Network Actor/Linked Network Actor.lvlib"/>
				<Item Name="TestLNA.lvlib" Type="Library" URL="../Packages/CSPP_LNA/TestLNA/Test LNA/TestLNA.lvlib"/>
			</Item>
			<Item Name="RT" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="CSPP-Core-SV-RT.lvlib" Type="Library" URL="../Packages/CSPP_RT/CSPP-Core-SV-RT.lvlib"/>
				<Item Name="CSPP-RTSystemHealth-SV.lvlib" Type="Library" URL="../Packages/CSPP_RT/CSPP_RTSystemHealth/CSPP-RTSystemHealth-SV.lvlib"/>
				<Item Name="CSPP_RT-Main.vi" Type="VI" URL="../Packages/CSPP_RT/CSPP_RT-Main.vi"/>
				<Item Name="CSPP_RTBaseContent.vi" Type="VI" URL="../Packages/CSPP_RT/CSPP_RTBaseContent.vi"/>
				<Item Name="CSPP_RTCoreContent.vi" Type="VI" URL="../Packages/CSPP_RT/CSPP_RTCoreContent.vi"/>
				<Item Name="CSPP_RTClasses.lvlib" Type="Library" URL="../Packages/CSPP_RT/CSPP_RTClasses/CSPP_RTClasses.lvlib"/>
				<Item Name="CSPP_RTMain.lvlib" Type="Library" URL="../Packages/CSPP_RT/CSPP_RTMain.lvlib"/>
				<Item Name="CSPP_RTRoot.lvlib" Type="Library" URL="../Packages/CSPP_RT/CSPP_RTRoot/CSPP_RTRoot.lvlib"/>
				<Item Name="CSPP_RTSystemHealth.lvlib" Type="Library" URL="../Packages/CSPP_RT/CSPP_RTSystemHealth/CSPP_RTSystemHealth.lvlib"/>
				<Item Name="CSPP_RTWatchdog.lvlib" Type="Library" URL="../Packages/CSPP_RT/CSPP_RTWatchdog/CSPP_RTWatchdog.lvlib"/>
			</Item>
			<Item Name="Syslog" Type="Folder">
				<Item Name="CSPP_Syslog.lvlib" Type="Library" URL="../Packages/CSPP_Syslog/CSPP_Syslog.lvlib"/>
			</Item>
		</Item>
		<Item Name="GDP" Type="Folder">
			<Item Name="GDP-9503-Support.lvlib" Type="Library" URL="../Packages/GDP-NI9503-Stepper-Drive/GDP-9503-Support.lvlib"/>
			<Item Name="GDP-RT-Content.vi" Type="VI" URL="../GDP-RT-Content.vi"/>
			<Item Name="GDP-RT-SV.lvlib" Type="Library" URL="../Packages/GDP_RT/GDP-RT-SV.lvlib"/>
			<Item Name="GDP_Controller.lvlib" Type="Library" URL="../Packages/GDP_RT/GDP_Controller/GDP_Controller.lvlib"/>
			<Item Name="GDP_FC302Actor.lvlib" Type="Library" URL="../Packages/GDP_RT/GDP_FC302Actor/GDP_FC302Actor.lvlib"/>
			<Item Name="GDP_FPGAActor.lvlib" Type="Library" URL="../Packages/GDP_RT/GDP_FPGAActor/GDP_FPGAActor.lvlib"/>
			<Item Name="GDP_RTController.lvlib" Type="Library" URL="../Packages/GDP_RT/GDP_RTController/GDP_RTController.lvlib"/>
			<Item Name="GDP_TensileStress.lvlib" Type="Library" URL="../Packages/GDP_RT/GDP_TensileStress/GDP_TensileStress.lvlib"/>
		</Item>
		<Item Name="Test" Type="Folder"/>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9068</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="FPGA Target" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{02550398-A1B6-4CEF-9C79-DF93AA276BFD}resource=/crio_M1/Phase A Neg;0;WriteMethodType=bool{058BDE3F-88B3-4BC7-9284-4ECC00B4386B}resource=/crio_M2/Phase A Pos;0;WriteMethodType=bool{08506A88-52D3-4F35-9F56-616C27516302}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"{09408E89-E652-409E-B86D-B14746BDDCBD}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"{0B20D082-D995-4F56-B137-4EED7F1C98C7}resource=/crio_Mod2/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{10482DE3-DA6D-4C28-ADB7-C4812FBEAAF7}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"{1214AFBB-D85C-4AA1-9C08-CCC33B90E5AC}resource=/crio_M2/Phase B Neg;0;WriteMethodType=bool{132E0FE4-79E1-449D-BA05-4A0DDD3B783E}resource=/crio_M2/Phase B Current;0;ReadMethodType=I16{1548036D-7578-4683-AE82-2C01461CC92E}resource=/Scan Clock;0;ReadMethodType=bool{1707A7DA-C8FF-4AE9-9571-D0C25A656F06}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"{1BCA61B6-B033-4862-AE6B-F3974F241395}resource=/crio_Mod3/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{237EC0F0-D8AC-4CF9-BD8E-7B197B44BA59}resource=/crio_Mod2/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{2490A2A0-454D-41BA-82B3-10C02322DDE2}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{2B5D984F-C44D-4E78-91D0-C00E6CB3CD16}resource=/crio_M3/User LED;0;WriteMethodType=bool{2EEF1415-43AB-4A03-ACB7-353F306B4C31}resource=/crio_Mod2/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{30989C7F-07D4-45D0-9669-91631476E472}resource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16{34016134-EFFB-427B-92B9-A390FCB1999D}resource=/crio_Mod2/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{361AE013-7ED9-4DE8-8B42-DF2CD1B121AC}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{3687226E-4FA3-4FF3-82C5-ED5250D2293F}resource=/crio_Mod2/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{37FDD58F-9B90-40BD-893F-EF63427B76C3}resource=/crio_Mod2/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{3A3D24A1-C56B-4B7C-BD02-4E6495C46441}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]{3A4F20B2-54F1-4C8E-AC7C-25E3D7E1B277}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"{3F95063F-1A61-407D-A083-AF449C59163D}resource=/crio_M1/Phase A Current;0;ReadMethodType=I16{3FC6E464-F645-4A65-9DAB-175729257192}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"{429AEF87-B7DD-428A-87E7-3F85564563F8}resource=/crio_Mod2/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{441AFF99-C093-42C6-91B5-42379FD4413F}resource=/crio_Mod2/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{481F5DE4-147A-4B02-8215-E4B688C4D5D0}resource=/crio_Mod2/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{485DCDCF-2342-4B5B-8E50-A99C9A39851C}resource=/crio_Mod2/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{51CA8154-8E5A-4440-9247-3F0E954B3978}resource=/crio_Mod2/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{54517700-4F75-47B1-B16E-8C83AFA73D57}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"{54860F6A-7B96-4643-B9DC-8A9469238290}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]{5626F30A-B1DB-454C-8C69-06F05EEC5AAF}resource=/crio_Mod1/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{5C05DF15-7578-4735-BCAC-A47E615A44F7}resource=/crio_Mod2/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{5E32D8CA-7BB4-4736-9064-3B6BA23A1CA5}resource=/crio_Mod2/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{6396CFB5-E7AE-449B-B10E-A86149AE07E3}resource=/crio_Mod2/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{639DF46F-CB5B-4B18-9DA6-80E77DE9DC9B}resource=/crio_M3/Phase B Pos;0;WriteMethodType=bool{6555E161-D4A0-4E9C-B132-4AD943FD368E}resource=/crio_Mod2/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{6642D5DE-E3C7-4337-A072-B7078CE95BC1}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"{6674BB18-5278-497D-8601-1C1FE98907A1}resource=/crio_Mod4/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{6965501E-6186-44CF-93CD-DC058177B825}resource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8{6F24328F-5E56-4F1A-A6E5-0B2C5F8B6D6B}resource=/crio_Mod2/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{6F24EE04-039F-4871-AAB7-A8CF828E39E1}resource=/crio_Mod4/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{70661257-552A-40FE-A499-276605217FA9}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{719E76BA-EBDC-4DEF-AA18-B8ED8629348F}resource=/crio_Mod2/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{75AA0BA2-B0F8-4DDF-850E-3A44B07E3689}resource=/crio_M2/Phase A Neg;0;WriteMethodType=bool{78E5AA40-08D0-4542-AADF-2D1A4A4B6B65}resource=/crio_Mod2/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{7CF169EF-DE09-4550-AC00-2435990E45ED}resource=/crio_Mod4/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{7DBD604C-9963-4EC8-8A51-68C70995D749}resource=/crio_Mod2/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{7EF00208-AFBC-43D0-841E-C3ABDEA7EF6C}resource=/crio_M3/Phase B Neg;0;WriteMethodType=bool{7FD072A4-12EE-40C9-A218-BBEE06C8130B}resource=/crio_Mod3/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{806802A4-D501-46AD-9CA9-824AB2ED379D}resource=/crio_Mod1/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{8177E0AB-2B4F-415D-9939-C5491C6D39CF}resource=/crio_Mod2/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{82036ED2-C216-425E-9DFE-F51C0BAC2180}resource=/crio_M3/Phase A Pos;0;WriteMethodType=bool{837BE5D0-A9F3-45AB-82D2-E67B283EF0CA}resource=/crio_Mod2/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{850D9256-B9D6-4AB6-8179-AB3537E2D8CB}resource=/crio_Mod2/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{8568BFC9-8DCD-4F23-8494-E43176C9E5C3}resource=/crio_Mod2/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{88E0DF76-AFDB-41D5-8130-AAAD61AEDD8A}resource=/crio_M3/Phase B Current;0;ReadMethodType=I16{9223A388-6F6A-4D31-A291-2784B0CB8903}resource=/crio_M2/Phase B Pos;0;WriteMethodType=bool{955B15E6-9AB2-4E8C-A935-0F2BD23ED93B}resource=/crio_M1/Phase B Neg;0;WriteMethodType=bool{96A860BC-AF35-4B86-82FA-39A05C4E8CD7}resource=/crio_Mod3/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{96AE89FD-ABB2-4CE3-B390-5F5898D41373}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"{9D85F2F0-9E2E-4D26-B7A8-5D987E9BA705}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=11111111000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{9FC23348-44C2-43B8-BF0F-3FCE1E0013E5}resource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16{A11D506F-C948-43AF-A68C-A447A4A1F44A}resource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16{A3971553-EC84-4A75-80F8-06CD98F77588}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"{A4B8A7DD-2CE5-436F-864B-3114976FAC9B}resource=/crio_Mod2/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{A9F77056-0701-4FE8-8AB9-A18A35685E83}resource=/crio_Mod2/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{AA169D9B-1BC5-4117-B106-C6215E025FF7}resource=/crio_M2/Phase A Current;0;ReadMethodType=I16{AC84A79A-185B-4F92-A4A1-C5C5F46C1EE1}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]{ACF8A1DA-234A-42C3-B9E7-F038BAB1574D}resource=/crio_Mod3/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{AD23AAC2-D3D2-471C-B939-D107D00A725D}resource=/crio_Mod2/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{AF06535A-D80B-49C1-8FDA-C210EF90E707}resource=/crio_Mod2/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{B0CBCE14-0CAD-439A-9609-88C13A11803D}resource=/crio_Mod2/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{B9E41DE2-DF22-465B-BD03-5A0ED7E5102A}resource=/crio_Mod2/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{BBB4F19B-44C7-43D8-BD73-070187C0B20A}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"{BBEA904A-1851-43F9-8340-4064EAD8F848}resource=/crio_Mod1/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{BF264139-8A0E-43F2-8345-1FE64D713070}resource=/crio_M1/Phase B Pos;0;WriteMethodType=bool{C32133D7-2F1A-432B-A4E7-089FFEAADFE2}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"{C399270A-4063-4370-AC26-B4A5D53DAB6E}resource=/crio_M3/Phase A Current;0;ReadMethodType=I16{C71E3CFF-83EF-4D31-B8AB-C31834AE77C1}resource=/crio_M2/User LED;0;WriteMethodType=bool{C7AB1B2B-AB8C-4247-B90B-42134FB4FB83}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C92ABDED-E273-4280-8EF8-DFC5033D84B5}resource=/crio_Mod2/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{CF8BEFB7-13A2-4881-AF4E-F70188FF30F9}resource=/crio_Mod2/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{DABE9BA0-57D6-42B1-8E22-56BEEEB4EF6F}resource=/crio_M3/Phase A Neg;0;WriteMethodType=bool{DD9CC897-51B6-4A6F-98BE-DA4E38B602A4}resource=/crio_Mod2/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{E0406E62-41DF-4D87-83B3-F8FE5586682D}"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"{E44F1919-A5D1-4536-A0C4-0AFA4A31BE6E}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{E6413438-5460-495A-8871-58EB1367056B}resource=/crio_M1/User LED;0;WriteMethodType=bool{E66AF48B-974A-4EBE-AAA0-70B1E32B44FC}resource=/crio_Mod2/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{E7B6E087-570B-43D0-8D8B-C9173097161E}resource=/crio_Mod2/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{E89C67BF-174D-4433-9378-3725F2C816BA}resource=/crio_Mod2/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{ECCA62FA-00C1-4A0E-99F4-70384ADFD355}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]{EE5246F0-E84E-40EB-AF29-B574F8C61DDA}resource=/crio_M1/Phase B Current;0;ReadMethodType=I16{F22DD7F2-6215-4A9C-BD8A-B2343037F501}resource=/crio_Mod2/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{F2F38582-F589-4A14-A8AD-0D75B742231E}resource=/crio_Mod1/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctl{F342E506-B15C-4DA0-B52A-BD0C707A3031}"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"{F521FEE5-F3F3-453D-8780-676F7EE5E831}resource=/crio_Mod2/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{F888E76B-614E-4BEA-BF79-1823BC8272C8}"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"{FA0DE78C-DBE3-42AA-BFB1-37D46308A7EC}resource=/crio_Mod4/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctl{FB7496E9-2E2D-48E7-9D96-CC292982CC6A}resource=/Chassis Temperature;0;ReadMethodType=i16{FBE2A1E5-DAA9-4BD6-A4B9-CCE1775EE2EF}resource=/crio_M1/Phase A Pos;0;WriteMethodType=boolcRIO-9068/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9068/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Encoder A+resource=/crio_Mod2/DIO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder A-resource=/crio_Mod2/DIO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder B+resource=/crio_Mod2/DIO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder B-resource=/crio_Mod2/DIO3;0;ReadMethodType=bool;WriteMethodType=boolEncoder Zresource=/crio_Mod2/DIO4;0;ReadMethodType=bool;WriteMethodType=boolM1/Phase A Currentresource=/crio_M1/Phase A Current;0;ReadMethodType=I16M1/Phase A Negresource=/crio_M1/Phase A Neg;0;WriteMethodType=boolM1/Phase A Posresource=/crio_M1/Phase A Pos;0;WriteMethodType=boolM1/Phase B Currentresource=/crio_M1/Phase B Current;0;ReadMethodType=I16M1/Phase B Negresource=/crio_M1/Phase B Neg;0;WriteMethodType=boolM1/Phase B Posresource=/crio_M1/Phase B Pos;0;WriteMethodType=boolM1/User LEDresource=/crio_M1/User LED;0;WriteMethodType=boolM1/Vsup Voltageresource=/crio_M1/Vsup Voltage;0;ReadMethodType=u16M1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9503[crioConfig.End]M1_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"M1_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"M1_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"M1_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"M1_Velocity"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"M2/Phase A Currentresource=/crio_M2/Phase A Current;0;ReadMethodType=I16M2/Phase A Negresource=/crio_M2/Phase A Neg;0;WriteMethodType=boolM2/Phase A Posresource=/crio_M2/Phase A Pos;0;WriteMethodType=boolM2/Phase B Currentresource=/crio_M2/Phase B Current;0;ReadMethodType=I16M2/Phase B Negresource=/crio_M2/Phase B Neg;0;WriteMethodType=boolM2/Phase B Posresource=/crio_M2/Phase B Pos;0;WriteMethodType=boolM2/User LEDresource=/crio_M2/User LED;0;WriteMethodType=boolM2/Vsup Voltageresource=/crio_M2/Vsup Voltage;0;ReadMethodType=u16M2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9503[crioConfig.End]M2_Acceleration"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"M2_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"M2_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"M2_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"M2_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"M3/Phase A Currentresource=/crio_M3/Phase A Current;0;ReadMethodType=I16M3/Phase A Negresource=/crio_M3/Phase A Neg;0;WriteMethodType=boolM3/Phase A Posresource=/crio_M3/Phase A Pos;0;WriteMethodType=boolM3/Phase B Currentresource=/crio_M3/Phase B Current;0;ReadMethodType=I16M3/Phase B Negresource=/crio_M3/Phase B Neg;0;WriteMethodType=boolM3/Phase B Posresource=/crio_M3/Phase B Pos;0;WriteMethodType=boolM3/User LEDresource=/crio_M3/User LED;0;WriteMethodType=boolM3/Vsup Voltageresource=/crio_M3/Vsup Voltage;0;ReadMethodType=u16M3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 6,crio.Type=NI 9503[crioConfig.End]M3_Acceleration"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"M3_ERC"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"M3_Fault"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"M3_Status"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"M3_Velocity"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"Mod1/AO0resource=/crio_Mod1/AO0;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod1/AO1resource=/crio_Mod1/AO1;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod1/AO2resource=/crio_Mod1/AO2;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod1/AO3resource=/crio_Mod1/AO3;0;WriteMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9269,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.HotSwapMode=0,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/DIO10resource=/crio_Mod2/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO11resource=/crio_Mod2/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO12resource=/crio_Mod2/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO13resource=/crio_Mod2/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO14resource=/crio_Mod2/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO15:8resource=/crio_Mod2/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod2/DIO15resource=/crio_Mod2/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO16resource=/crio_Mod2/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO17resource=/crio_Mod2/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO18resource=/crio_Mod2/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO19resource=/crio_Mod2/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO20resource=/crio_Mod2/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO21resource=/crio_Mod2/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO22resource=/crio_Mod2/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO23:16resource=/crio_Mod2/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod2/DIO23resource=/crio_Mod2/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO24resource=/crio_Mod2/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO25resource=/crio_Mod2/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO26resource=/crio_Mod2/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO27resource=/crio_Mod2/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO28resource=/crio_Mod2/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO29resource=/crio_Mod2/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO30resource=/crio_Mod2/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO31:0resource=/crio_Mod2/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod2/DIO31:24resource=/crio_Mod2/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod2/DIO31resource=/crio_Mod2/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO5resource=/crio_Mod2/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO6resource=/crio_Mod2/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO7:0resource=/crio_Mod2/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod2/DIO7resource=/crio_Mod2/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO8resource=/crio_Mod2/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod2/DIO9resource=/crio_Mod2/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=11111111000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Mod3/AI0resource=/crio_Mod3/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod3/AI1resource=/crio_Mod3/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod3/AI2resource=/crio_Mod3/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod3/AI3resource=/crio_Mod3/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_20_5.ctlMod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9215,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod4/RTD0resource=/crio_Mod4/RTD0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlMod4/RTD1resource=/crio_Mod4/RTD1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlMod4/RTD2resource=/crio_Mod4/RTD2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlMod4/RTD3resource=/crio_Mod4/RTD3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_24_10.ctlMod4[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9217,cRIOModule.AI0.DegreeRange=2,cRIOModule.AI0.RTD_A=3,908300E-3,cRIOModule.AI0.RTD_B=-5,775000E-7,cRIOModule.AI0.RTD_C=-4,183000E-12,cRIOModule.AI0.RTD_Ro=1,000000E+2,cRIOModule.AI0.RTDType=1,cRIOModule.AI1.DegreeRange=2,cRIOModule.AI1.RTD_A=3,908300E-3,cRIOModule.AI1.RTD_B=-5,775000E-7,cRIOModule.AI1.RTD_C=-4,183000E-12,cRIOModule.AI1.RTD_Ro=1,000000E+2,cRIOModule.AI1.RTDType=1,cRIOModule.AI2.DegreeRange=2,cRIOModule.AI2.RTD_A=3,908300E-3,cRIOModule.AI2.RTD_B=-5,775000E-7,cRIOModule.AI2.RTD_C=-4,183000E-12,cRIOModule.AI2.RTD_Ro=1,000000E+2,cRIOModule.AI2.RTDType=1,cRIOModule.AI3.DegreeRange=2,cRIOModule.AI3.RTD_A=3,908300E-3,cRIOModule.AI3.RTD_B=-5,775000E-7,cRIOModule.AI3.RTD_C=-4,183000E-12,cRIOModule.AI3.RTD_Ro=1,000000E+2,cRIOModule.AI3.RTDType=1,cRIOModule.Conversion Time=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDresource=/USER FPGA LED;0;ReadMethodType=u8;WriteMethodType=u8</Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9068/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9068FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="Target Class" Type="Str">cRIO-9068</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="IO" Type="Folder">
					<Item Name="Chassis I/O" Type="Folder">
						<Item Name="Chassis Temperature" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{FB7496E9-2E2D-48E7-9D96-CC292982CC6A}</Property>
						</Item>
						<Item Name="Scan Clock" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{1548036D-7578-4683-AE82-2C01461CC92E}</Property>
						</Item>
						<Item Name="Sleep" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{70661257-552A-40FE-A499-276605217FA9}</Property>
						</Item>
						<Item Name="System Reset" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{E44F1919-A5D1-4536-A0C4-0AFA4A31BE6E}</Property>
						</Item>
						<Item Name="USER FPGA LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/USER FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6965501E-6186-44CF-93CD-DC058177B825}</Property>
						</Item>
					</Item>
					<Item Name="M1" Type="Folder">
						<Item Name="M1/Phase A Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{3F95063F-1A61-407D-A083-AF449C59163D}</Property>
						</Item>
						<Item Name="M1/Phase A Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{02550398-A1B6-4CEF-9C79-DF93AA276BFD}</Property>
						</Item>
						<Item Name="M1/Phase A Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{FBE2A1E5-DAA9-4BD6-A4B9-CCE1775EE2EF}</Property>
						</Item>
						<Item Name="M1/Phase B Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{EE5246F0-E84E-40EB-AF29-B574F8C61DDA}</Property>
						</Item>
						<Item Name="M1/Phase B Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{955B15E6-9AB2-4E8C-A935-0F2BD23ED93B}</Property>
						</Item>
						<Item Name="M1/Phase B Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{BF264139-8A0E-43F2-8345-1FE64D713070}</Property>
						</Item>
						<Item Name="M1/User LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{E6413438-5460-495A-8871-58EB1367056B}</Property>
						</Item>
						<Item Name="M1/Vsup Voltage" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M1/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{30989C7F-07D4-45D0-9669-91631476E472}</Property>
						</Item>
					</Item>
					<Item Name="M2" Type="Folder">
						<Item Name="M2/Phase A Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{AA169D9B-1BC5-4117-B106-C6215E025FF7}</Property>
						</Item>
						<Item Name="M2/Phase A Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{75AA0BA2-B0F8-4DDF-850E-3A44B07E3689}</Property>
						</Item>
						<Item Name="M2/Phase A Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{058BDE3F-88B3-4BC7-9284-4ECC00B4386B}</Property>
						</Item>
						<Item Name="M2/Phase B Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{132E0FE4-79E1-449D-BA05-4A0DDD3B783E}</Property>
						</Item>
						<Item Name="M2/Phase B Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{1214AFBB-D85C-4AA1-9C08-CCC33B90E5AC}</Property>
						</Item>
						<Item Name="M2/Phase B Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{9223A388-6F6A-4D31-A291-2784B0CB8903}</Property>
						</Item>
						<Item Name="M2/User LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{C71E3CFF-83EF-4D31-B8AB-C31834AE77C1}</Property>
						</Item>
						<Item Name="M2/Vsup Voltage" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M2/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{A11D506F-C948-43AF-A68C-A447A4A1F44A}</Property>
						</Item>
					</Item>
					<Item Name="M3" Type="Folder">
						<Item Name="M3/Phase A Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase A Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{C399270A-4063-4370-AC26-B4A5D53DAB6E}</Property>
						</Item>
						<Item Name="M3/Phase A Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase A Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{DABE9BA0-57D6-42B1-8E22-56BEEEB4EF6F}</Property>
						</Item>
						<Item Name="M3/Phase A Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase A Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{82036ED2-C216-425E-9DFE-F51C0BAC2180}</Property>
						</Item>
						<Item Name="M3/Phase B Current" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase B Current</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{88E0DF76-AFDB-41D5-8130-AAAD61AEDD8A}</Property>
						</Item>
						<Item Name="M3/Phase B Neg" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase B Neg</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{7EF00208-AFBC-43D0-841E-C3ABDEA7EF6C}</Property>
						</Item>
						<Item Name="M3/Phase B Pos" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Phase B Pos</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{639DF46F-CB5B-4B18-9DA6-80E77DE9DC9B}</Property>
						</Item>
						<Item Name="M3/User LED" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/User LED</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{2B5D984F-C44D-4E78-91D0-C00E6CB3CD16}</Property>
						</Item>
						<Item Name="M3/Vsup Voltage" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_M3/Vsup Voltage</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{9FC23348-44C2-43B8-BF0F-3FCE1E0013E5}</Property>
						</Item>
					</Item>
					<Item Name="Mod1" Type="Folder">
						<Item Name="Mod1/AO0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/AO0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{BBEA904A-1851-43F9-8340-4064EAD8F848}</Property>
						</Item>
						<Item Name="Mod1/AO1" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/AO1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{F2F38582-F589-4A14-A8AD-0D75B742231E}</Property>
						</Item>
						<Item Name="Mod1/AO2" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/AO2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{806802A4-D501-46AD-9CA9-824AB2ED379D}</Property>
						</Item>
						<Item Name="Mod1/AO3" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/AO3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{5626F30A-B1DB-454C-8C69-06F05EEC5AAF}</Property>
						</Item>
					</Item>
					<Item Name="Mod2" Type="Folder">
						<Item Name="Encoder A+" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{37FDD58F-9B90-40BD-893F-EF63427B76C3}</Property>
						</Item>
						<Item Name="Encoder A-" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{E66AF48B-974A-4EBE-AAA0-70B1E32B44FC}</Property>
						</Item>
						<Item Name="Encoder B+" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{237EC0F0-D8AC-4CF9-BD8E-7B197B44BA59}</Property>
						</Item>
						<Item Name="Encoder B-" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{34016134-EFFB-427B-92B9-A390FCB1999D}</Property>
						</Item>
						<Item Name="Encoder Z" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{5C05DF15-7578-4735-BCAC-A47E615A44F7}</Property>
						</Item>
						<Item Name="Mod2/DIO5" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{7DBD604C-9963-4EC8-8A51-68C70995D749}</Property>
						</Item>
						<Item Name="Mod2/DIO6" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{E7B6E087-570B-43D0-8D8B-C9173097161E}</Property>
						</Item>
						<Item Name="Mod2/DIO7" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{DD9CC897-51B6-4A6F-98BE-DA4E38B602A4}</Property>
						</Item>
						<Item Name="Mod2/DIO7:0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6555E161-D4A0-4E9C-B132-4AD943FD368E}</Property>
						</Item>
						<Item Name="Mod2/DIO8" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{485DCDCF-2342-4B5B-8E50-A99C9A39851C}</Property>
						</Item>
						<Item Name="Mod2/DIO9" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{E89C67BF-174D-4433-9378-3725F2C816BA}</Property>
						</Item>
						<Item Name="Mod2/DIO10" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{C92ABDED-E273-4280-8EF8-DFC5033D84B5}</Property>
						</Item>
						<Item Name="Mod2/DIO11" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{719E76BA-EBDC-4DEF-AA18-B8ED8629348F}</Property>
						</Item>
						<Item Name="Mod2/DIO12" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{481F5DE4-147A-4B02-8215-E4B688C4D5D0}</Property>
						</Item>
						<Item Name="Mod2/DIO13" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{5E32D8CA-7BB4-4736-9064-3B6BA23A1CA5}</Property>
						</Item>
						<Item Name="Mod2/DIO14" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{AD23AAC2-D3D2-471C-B939-D107D00A725D}</Property>
						</Item>
						<Item Name="Mod2/DIO15" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{441AFF99-C093-42C6-91B5-42379FD4413F}</Property>
						</Item>
						<Item Name="Mod2/DIO15:8" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{0B20D082-D995-4F56-B137-4EED7F1C98C7}</Property>
						</Item>
						<Item Name="Mod2/DIO16" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{51CA8154-8E5A-4440-9247-3F0E954B3978}</Property>
						</Item>
						<Item Name="Mod2/DIO17" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{B9E41DE2-DF22-465B-BD03-5A0ED7E5102A}</Property>
						</Item>
						<Item Name="Mod2/DIO18" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{837BE5D0-A9F3-45AB-82D2-E67B283EF0CA}</Property>
						</Item>
						<Item Name="Mod2/DIO19" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{B0CBCE14-0CAD-439A-9609-88C13A11803D}</Property>
						</Item>
						<Item Name="Mod2/DIO20" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{A9F77056-0701-4FE8-8AB9-A18A35685E83}</Property>
						</Item>
						<Item Name="Mod2/DIO21" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{A4B8A7DD-2CE5-436F-864B-3114976FAC9B}</Property>
						</Item>
						<Item Name="Mod2/DIO22" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{F521FEE5-F3F3-453D-8780-676F7EE5E831}</Property>
						</Item>
						<Item Name="Mod2/DIO23" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{3687226E-4FA3-4FF3-82C5-ED5250D2293F}</Property>
						</Item>
						<Item Name="Mod2/DIO23:16" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{F22DD7F2-6215-4A9C-BD8A-B2343037F501}</Property>
						</Item>
						<Item Name="Mod2/DIO24" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{8568BFC9-8DCD-4F23-8494-E43176C9E5C3}</Property>
						</Item>
						<Item Name="Mod2/DIO25" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{78E5AA40-08D0-4542-AADF-2D1A4A4B6B65}</Property>
						</Item>
						<Item Name="Mod2/DIO26" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{AF06535A-D80B-49C1-8FDA-C210EF90E707}</Property>
						</Item>
						<Item Name="Mod2/DIO27" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{8177E0AB-2B4F-415D-9939-C5491C6D39CF}</Property>
						</Item>
						<Item Name="Mod2/DIO28" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6396CFB5-E7AE-449B-B10E-A86149AE07E3}</Property>
						</Item>
						<Item Name="Mod2/DIO29" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{CF8BEFB7-13A2-4881-AF4E-F70188FF30F9}</Property>
						</Item>
						<Item Name="Mod2/DIO30" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{2EEF1415-43AB-4A03-ACB7-353F306B4C31}</Property>
						</Item>
						<Item Name="Mod2/DIO31" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{850D9256-B9D6-4AB6-8179-AB3537E2D8CB}</Property>
						</Item>
						<Item Name="Mod2/DIO31:0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6F24328F-5E56-4F1A-A6E5-0B2C5F8B6D6B}</Property>
						</Item>
						<Item Name="Mod2/DIO31:24" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DIO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{429AEF87-B7DD-428A-87E7-3F85564563F8}</Property>
						</Item>
					</Item>
					<Item Name="Mod3" Type="Folder">
						<Item Name="Mod3/AI0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{ACF8A1DA-234A-42C3-B9E7-F038BAB1574D}</Property>
						</Item>
						<Item Name="Mod3/AI1" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{1BCA61B6-B033-4862-AE6B-F3974F241395}</Property>
						</Item>
						<Item Name="Mod3/AI2" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{7FD072A4-12EE-40C9-A218-BBEE06C8130B}</Property>
						</Item>
						<Item Name="Mod3/AI3" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{96A860BC-AF35-4B86-82FA-39A05C4E8CD7}</Property>
						</Item>
					</Item>
					<Item Name="Mod4" Type="Folder">
						<Item Name="Mod4/RTD0" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/RTD0</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{7CF169EF-DE09-4550-AC00-2435990E45ED}</Property>
						</Item>
						<Item Name="Mod4/RTD1" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/RTD1</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6674BB18-5278-497D-8601-1C1FE98907A1}</Property>
						</Item>
						<Item Name="Mod4/RTD2" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/RTD2</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{6F24EE04-039F-4871-AAB7-A8CF828E39E1}</Property>
						</Item>
						<Item Name="Mod4/RTD3" Type="Elemental IO">
							<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod4/RTD3</Value>
   </Attribute>
</AttributeSet>
</Property>
							<Property Name="FPGA.PersistentID" Type="Str">{FA0DE78C-DBE3-42AA-BFB1-37D46308A7EC}</Property>
						</Item>
					</Item>
				</Item>
				<Item Name="Register" Type="Folder">
					<Item Name="M1_Acceleration" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Acceleration;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E0406E62-41DF-4D87-83B3-F8FE5586682D}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000000000000000000000000000000000000000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_ERC" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_ERC;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A3971553-EC84-4A75-80F8-06CD98F77588}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_Fault" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Fault;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C32133D7-2F1A-432B-A4E7-089FFEAADFE2}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_Status" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=BF09508C8DEF1CF2DA5A303ABEB12465;Name=Motor-0_Status;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{09408E89-E652-409E-B86D-B14746BDDCBD}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M1_Velocity" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=F52AEAD55C71D643510D5F624F787E11;Name=Motor-0_Velocity;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{96AE89FD-ABB2-4CE3-B390-5F5898D41373}</Property>
						<Property Name="Initial Data" Type="Str">0000000000000000000000000000000000000000000000000000000000000000</Property>
						<Property Name="Initialized" Type="Bool">true</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Acceleration" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Acceleration;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BBB4F19B-44C7-43D8-BD73-070187C0B20A}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001000100000020FFFFFFFFFFFFFFFF0000003F0000001F7FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_ERC" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_ERC;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{10482DE3-DA6D-4C28-ADB7-C4812FBEAAF7}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Fault" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Fault;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6642D5DE-E3C7-4337-A072-B7078CE95BC1}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Status" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-3_Status;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{54517700-4F75-47B1-B16E-8C83AFA73D57}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M2_Velocity" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-3_Velocity;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F888E76B-614E-4BEA-BF79-1823BC8272C8}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Acceleration" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Acceleration;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3A4F20B2-54F1-4C8E-AC7C-25E3D7E1B277}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_ERC" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_ERC;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F342E506-B15C-4DA0-B52A-BD0C707A3031}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Fault" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Fault;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3FC6E464-F645-4A65-9DAB-175729257192}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Status" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=100080000000000100094006000355313600010000000000000000;InitDataHash=;Name=Motor-4_Status;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">6</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1707A7DA-C8FF-4AE9-9571-D0C25A656F06}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">100080000000000100094006000355313600010000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
					<Item Name="M3_Velocity" Type="FPGA Register">
						<Property Name="Arbitration For Write" Type="UInt">1</Property>
						<Property Name="Compile Config String" Type="Str">"DataType=1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000;InitDataHash=;Name=Motor-4_Velocity;WriteArb=1"</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{08506A88-52D3-4F35-9F56-616C27516302}</Property>
						<Property Name="Initial Data" Type="Str"></Property>
						<Property Name="Initialized" Type="Bool">false</Property>
						<Property Name="InitVIPath" Type="Str"></Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510040000000200001004000000020800000000000000000010040000000207FFFFFFFFFFFFFFF00000001FFFFFFE1000000000000000100010000000000000000000000000000</Property>
						<Property Name="Valid" Type="Bool">true</Property>
						<Property Name="Version" Type="Int">1</Property>
					</Item>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{361AE013-7ED9-4DE8-8B42-DF2CD1B121AC}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="GDP-9503.lvlib" Type="Library" URL="../Packages/GDP-NI9503-Stepper-Drive/GDP-9503.lvlib"/>
				<Item Name="GDP_FPGA.lvlib" Type="Library" URL="../Packages/GDP_RT/GDP_FPGA/GDP_FPGA.lvlib"/>
				<Item Name="IP Builder" Type="IP Builder Target">
					<Item Name="Dependencies" Type="Dependencies"/>
					<Item Name="Build Specifications" Type="Build"/>
				</Item>
				<Item Name="M1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 8</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{54860F6A-7B96-4643-B9DC-8A9469238290}</Property>
				</Item>
				<Item Name="M2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 7</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{AC84A79A-185B-4F92-A4A1-C5C5F46C1EE1}</Property>
				</Item>
				<Item Name="M3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 6</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9503</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{ECCA62FA-00C1-4A0E-99F4-70384ADFD355}</Property>
				</Item>
				<Item Name="Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9269</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.HotSwapMode" Type="Str">0</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3A3D24A1-C56B-4B7C-BD02-4E6495C46441}</Property>
				</Item>
				<Item Name="Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9403</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.Initial Line Direction" Type="Str">11111111000000000000000000000000</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{9D85F2F0-9E2E-4D26-B7A8-5D987E9BA705}</Property>
				</Item>
				<Item Name="Mod3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9215</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{C7AB1B2B-AB8C-4247-B90B-42134FB4FB83}</Property>
				</Item>
				<Item Name="Mod4" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 4</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9217</Property>
					<Property Name="cRIOModule.AI0.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI0.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI0.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI0.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI0.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI0.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.AI1.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI1.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI1.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI1.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI1.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI1.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.AI2.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI2.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI2.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI2.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI2.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI2.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.AI3.DegreeRange" Type="Str">2</Property>
					<Property Name="cRIOModule.AI3.RTD_A" Type="Str">3,908300E-3</Property>
					<Property Name="cRIOModule.AI3.RTD_B" Type="Str">-5,775000E-7</Property>
					<Property Name="cRIOModule.AI3.RTD_C" Type="Str">-4,183000E-12</Property>
					<Property Name="cRIOModule.AI3.RTD_Ro" Type="Str">1,000000E+2</Property>
					<Property Name="cRIOModule.AI3.RTDType" Type="Str">1</Property>
					<Property Name="cRIOModule.Conversion Time" Type="Str">0</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2490A2A0-454D-41BA-82B3-10C02322DDE2}</Property>
				</Item>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
						<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
						<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					</Item>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="GDP_Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">GDP_Main</Property>
						<Property Name="Comp.BitfileName" Type="Str">GemDiscProductio_FPGATarget_GDPMain_dr4gtNyZOag.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">22</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str">AlternateReplication</Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str">SpreadLogic_high</Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str">MoreGlobalIterations</Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Optimize congestion</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">Packages/GDP_RT/GDP_FPGA/FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/User/Brand/LVP/Detlab/GemDiscProduction/Packages/GDP_RT/GDP_FPGA/FPGA Bitfiles/GemDiscProductio_FPGATarget_GDPMain_dr4gtNyZOag.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">Packages/GDP_RT/GDP_FPGA/FPGA Bitfiles/GemDiscProductio_FPGATarget_GDPMain_dr4gtNyZOag.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/C/User/Brand/LVP/Detlab/GemDiscProduction/GemDiscProduction-RT.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/RT CompactRIO Target/Chassis/FPGA Target/GDP_FPGA.lvlib/Main.vi</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Real-Time Scan Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.RSIModuleContainer</Property>
			</Item>
		</Item>
		<Item Name="cRIO-LV2016 Configuration.PNG" Type="Document" URL="../cRIO-LV2016 Configuration.PNG"/>
		<Item Name="cRIO-LV2017 Configuration.PNG" Type="Document" URL="../cRIO-LV2017 Configuration.PNG"/>
		<Item Name="GDP-RT-Main.vi" Type="VI" URL="../GDP-RT-Main.vi"/>
		<Item Name="GemDiscProduction-RT.ini" Type="Document" URL="../GemDiscProduction-RT.ini"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Flush And Wait Empty Condition.ctl" Type="VI" URL="/&lt;vilib&gt;/dex/Flush And Wait Empty Condition.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="lvpidtkt.dll" Type="Document" URL="/&lt;vilib&gt;/addons/control/pid/lvpidtkt.dll"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Modbus Master.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Modbus/master/Modbus Master.lvclass"/>
				<Item Name="ni_emb.dll" Type="Document" URL="/&lt;vilib&gt;/ni_emb.dll"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_PID__prctrl compat.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/control/pid/NI_PID__prctrl compat.lvlib"/>
				<Item Name="NI_PID_pid.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/control/pid/NI_PID_pid.lvlib"/>
				<Item Name="NI_Real-Time Target Support.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI_Real-Time Target Support.lvlib"/>
				<Item Name="NI_SoftMotion_MotorControlIP.lvlib" Type="Library" URL="/&lt;vilib&gt;/Motion/MotorControl/NI_SoftMotion_MotorControlIP.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="nisyscfg.lvlib" Type="Library" URL="/&lt;vilib&gt;/nisyscfg/nisyscfg.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="SubVIs.lvlib" Type="Library" URL="/&lt;vilib&gt;/Modbus/subvis/SubVIs.lvlib"/>
				<Item Name="Syslog Device Close.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Device Close.vi"/>
				<Item Name="Syslog Device Init.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Device Init.vi"/>
				<Item Name="Syslog Device Send.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/Syslog Device Send.vi"/>
				<Item Name="syslog_Device Function Engine.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/syslog_Device Function Engine.vi"/>
				<Item Name="syslog_device_functions.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/TypeDefs/syslog_device_functions.ctl"/>
				<Item Name="syslog_facility_codes.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/TypeDefs/syslog_facility_codes.ctl"/>
				<Item Name="syslog_Hostname.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/syslog_Hostname.vi"/>
				<Item Name="syslog_severity_codes.ctl" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/TypeDefs/syslog_severity_codes.ctl"/>
				<Item Name="syslog_Timestamp.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/_subVIs/syslog_Timestamp.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="util_My IP Address.vi" Type="VI" URL="/&lt;vilib&gt;/NI/syslog/examples/util/util_My IP Address.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Watchdog Acknowledge.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Acknowledge.vi"/>
				<Item Name="Watchdog Add Interrupt Action.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Add Interrupt Action.vi"/>
				<Item Name="Watchdog Add Reset Action.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Add Reset Action.vi"/>
				<Item Name="Watchdog Add Restart RTE Action.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Add Restart RTE Action.vi"/>
				<Item Name="Watchdog Add Trigger Action.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Add Trigger Action.vi"/>
				<Item Name="Watchdog attribute enum.ctl" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog attribute enum.ctl"/>
				<Item Name="Watchdog Configure.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Configure.vi"/>
				<Item Name="Watchdog Enable.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Enable.vi"/>
				<Item Name="Watchdog expiration actions.ctl" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog expiration actions.ctl"/>
				<Item Name="Watchdog Open.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Open.vi"/>
				<Item Name="Watchdog Reset.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Reset.vi"/>
				<Item Name="Watchdog Set Attribute.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Set Attribute.vi"/>
				<Item Name="Watchdog Start.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Start.vi"/>
				<Item Name="Watchdog status enum.ctl" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog status enum.ctl"/>
				<Item Name="Watchdog Whack.vi" Type="VI" URL="/&lt;vilib&gt;/RTwatchdog.llb/Watchdog Whack.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="GemDiscProductio_FPGATarget_GDPMain_dr4gtNyZOag.lvbitx" Type="Document" URL="../Packages/GDP_RT/GDP_FPGA/FPGA Bitfiles/GemDiscProductio_FPGATarget_GDPMain_dr4gtNyZOag.lvbitx"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niwd4c.dll" Type="Document" URL="niwd4c.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="GemDiskProduction-RT" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{EC7316C3-2110-4818-8E6D-CBF9914608AA}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D98A5213-24FE-4B4E-A1E9-465AEFFD8C37}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{DE3D3575-C396-41F0-AC94-0E811CEAE0E1}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">GemDiskProduction-RT</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/tmp/builds/NI_AB_PROJECTNAME</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{FB387662-2567-4F01-A2A7-7ABB375145A6}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/home/lvuser/natinst/bin</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/GemDiskProduction-RT/RT CompactRIO Target/GemDiskProduction-RT/home/lvuser/natinst/bin/GemDiskProduction-RT_GemDiskProduction-RT_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Destination[0].destName" Type="Str">startup.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/home/lvuser/natinst/bin/startup.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/home/lvuser/natinst/bin/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">C</Property>
				<Property Name="Destination[2].path" Type="Path">/C</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].itemID" Type="Str">{8B8D5B5A-D235-4A15-B1EB-DD005F73A41F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/RT CompactRIO Target/GDP-RT-Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref">/RT CompactRIO Target/GemDiscProduction-RT.ini</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">GemDiskProduction-RT</Property>
				<Property Name="TgtF_internalName" Type="Str">GemDiskProduction-RT</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">GemDiskProduction-RT</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{A978FB68-4354-404C-BF65-0BD6351C6B59}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">startup.rtexe</Property>
			</Item>
		</Item>
	</Item>
</Project>
