This is the Readme for the LabVIEW project "GemDiskProduction.lvproj".

Software zur Steuerung einer Produktionsanlage für gewickelte Faserverbundwerkstoffe, welche im Rahmen des PANDA-Projekts Anwendung finden wird.

- It is based on native LabVIEW classes and the Actor Framework.
- Is used CS++ and follows the KISS principle: "Keep It Smart & Simple"

The project GIT workflow contains five branches: 
- MainDev: main development line
- master: released versions

LabVIEW 2016 is currently used development.

Project is just started, so the most active branch is MainDev.

Related documents and information
================================= 
- README.md
- Release_Notes.md
- Contact: D.Krebs@gsi.de or H.Brand@gsi.de
- Download, bug reports... : To be added
- Documentation:
  - Refer to Documantation Folder
  - Project-Wiki: https://...
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview
  - *CS++*: http://github.com/HB-GSI/CSPP

GIT Submodules
=================================
Following git submodules are defined in this project.

- Packages/CSPP_Core
- Packages/CSPP_DeviceBase
- Packages/CSPP_DSC
- Packages/CSPP_LNA
- Packages/CSPP_ObjectManager
- Packages/CSPP_RT
- Packages/CSPP_Syslog
- Packages/CSPP_Utilities

Optional submodules available:

External Dependencies
=================================
Optional:

- Syslog; Refer to http://sine.ni.com/nips/cds/view/p/lang/de/nid/209116
- Linked Network Actor; Refer to http://forums.ni.com/t5/Actor-Framework-Documents/Linked-Network-Actor/ta-p/3513034
- Monitored Actor; Refer to https://decibel.ni.com/content/thread/18301 and http://lavag.org/topic/17056-monitoring-actors

Getting started:
=================================
- Create a project specific copy of "GemDiskProduction.lvproj"
- You need to create your project specific ini-file, like "GemDiskProduction.ini"
  - Sample ini-file should be available for all classes, either in the LV-Project or on disk in the corresponding class or package folder.
- You need to create and deploy your project specific shared Variable libraries.
  - Sample shared Variable libraries should be available for all concerned classes on disk in the corresponding class or package folder.
- Run your project specific "CS++Main.vi" or "CS++StartActor:Launch CS++StartActor.vi"
- Build application
  - The default build specification uses
    - "CS++StartActor:Launch CS++StartActor.vi" as startup-VI. It calls
    - "CS++UserContents.vi"; Include your project specific Content-VIs in a corresponding case of the conditional disable structure.
      This makes building an application convenient since the application builder can find all dependencies in the VI-Hierarchy.
  - Duplicate the default build specification and adapt it to your needs.


Author: D.Krebs@gsi.de, H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany
